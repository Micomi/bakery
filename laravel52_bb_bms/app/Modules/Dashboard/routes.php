<?php

Route::group(array('module' => 'Dashboard', 'namespace' => 'App\Modules\Dashboard\Controllers', 'middleware' => ['web', 'auth']), function() {

    Route::resource('/', 'DashboardController');
    
});	