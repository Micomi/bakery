<?php
namespace App;

class BmsApp  extends \Illuminate\Foundation\Application
{
    public function publicPath()
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'/../public_html';
    }
}