
<?php
return [
    'mainNavText' => 'MAIN NAVIGATION',
    'online' => 'Online',
    'toggleNav' => 'Toggle navigation',
    'profile' => 'Profile',
    'signOut' => 'Sign out',
];