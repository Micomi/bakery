<?php

return [
/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/
'title' => 'Login',
'loginToStartText' => 'Sign in to start your session',
'email' => 'Email',
'password' => 'Password',
'remember' => 'Remember Me',
'login' => 'Sign In',
'forgot' => 'Forgot Your Password?',
'registerText' => 'Register a new membership',
'failed' => 'These credentials do not match our records.',
'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
'fullName' => 'Full Name',
'confirmPassword' => 'Confirm password',
'iHaveMembership' => 'I already have a membership',
'register' => 'Register',
'resetPasswordTitle' => 'Reset Password',
'back' => 'Back',
];

