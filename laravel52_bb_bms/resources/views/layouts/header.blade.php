<!-- Logo -->
<a href="{{ url('/') }}" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>B</b>MS</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>BMS</b>beta</span>
</a>

<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">{{trans('global.toggleNav')}}</span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{asset('resources/img/logo.png')}}" class="user-image" alt="User Image">
                    <span class="hidden-xs">@yield('user-last-name') @yield('user-first-name') &nbsp;</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="@yield('user-image-href', asset('resources/img/logo.png'))" class="img-circle" alt="User Image">

                        <p>
                            @yield('user-last-name') @yield('user-first-name') - @yield('user-role')
                        </p>
                    </li>

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ url('/user/profile') }}" class="btn btn-default btn-flat">{{trans('global.profile')}}</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">{{trans('global.signOut')}}</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- Navbar Right Menu -->
</nav>