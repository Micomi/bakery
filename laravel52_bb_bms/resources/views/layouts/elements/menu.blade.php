<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('resources/img/logo.png')}}" class="img-circle">
            </div>
            <div class="pull-left info">
                <p>@yield('user-last-name') @yield('user-first-name')</p>
                <a href="#"><i class="fa fa-circle text-success"></i> {{trans('global.online')}}</a>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">{{trans('global.mainNavText')}}</li>

            @yield('main-menu', '
                <li class="active treeview">
                    <a href="#">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="#"><i class="fa fa-circle-o"></i> Dashboard v1</a></li>
                        <li class="active"><a href="#"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
                    </ul>
                </li>
            ')

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>