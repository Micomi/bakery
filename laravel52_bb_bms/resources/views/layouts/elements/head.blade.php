<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/node_modules/admin-lte/bootstrap/css/bootstrap.min.css')}}" />
<!-- Font Awesome -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/css/font-awesome.min.css')}}" />
<!-- Ionicons -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/css/ionicons.min.css')}}" />
<!-- jvectormap -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Theme style -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/node_modules/admin-lte/dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" type="text/css" href="{{asset('resources/node_modules/admin-lte/dist/css/skins/_all-skins.min.css')}}">

<link rel="stylesheet" type="text/css" href="{{asset('resources/node_modules/admin-lte/plugins/iCheck/square/blue.css')}}">


<link rel="stylesheet" type="text/css" href="{{asset('resources/css/main.css')}}" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script type="text/javascript" src="{{asset('resources/js/html5shiv.min.js')}}"></script>
<script type="text/javascript" src="{{asset('resources/js/respond.min.js')}}"></script>
<![endif]-->