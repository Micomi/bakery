<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>

        @include('layouts.elements.head')
    </head>


    <body  id="app-layout" class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                @include('layouts.header')
            </header>

            <!-- Left side column. contains the logo and sidebar -->
            @include('layouts.elements.menu')

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('title')
                    </h1>

                    <ol class="breadcrumb">
                        @yield('breadcrumb')
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                @include('layouts.footer')
            </footer>
        </div>

        <!-- jQuery 2.2.0 -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>

        <!-- Bootstrap 3.3.6 -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/bootstrap/js/bootstrap.min.js')}}"></script>

        <!-- FastClick -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/fastclick/fastclick.js')}}"></script>

        <!-- AdminLTE App -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/dist/js/app.min.js')}}"></script>

        <!-- Sparkline -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/sparkline/jquery.sparkline.min.js')}}"></script>

        <!-- jvectormap -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>

        <!-- SlimScroll 1.3.0 -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>

        <!-- ChartJS 1.0.1 -->
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/chartjs/Chart.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('resources/js/main.js')}}"></script>

    </body>

</html>

