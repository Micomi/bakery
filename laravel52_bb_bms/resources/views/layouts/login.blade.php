<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>@yield('title')</title>

        @include('layouts.elements.head')
    </head>

    <body class="hold-transition login-page">

        <div class="login-box">
            @yield('content')
        </div>

        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/jQuery/jQuery-2.2.0.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('resources/node_modules/admin-lte/plugins/iCheck/icheck.min.js')}}"></script>

        <script type="text/javascript" src="{{asset('resources/js/main.js')}}"></script>

    </body>
</html>