@extends('layouts.login')

@section('title', trans('auth.resetPasswordTitle'))

<!-- Main Content -->
@section('content')

    <div class="hold-transition register-page">
        <div class="register-box">
            <div class="register-logo">
                <a href="{{url('/')}}"><b>BMS</b>beta</a>
            </div>


            <div class="register-box-body">
                <p class="login-box-msg">{{trans('auth.resetPasswordTitle')}}</p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form role="form" method="POST" action="{{ url('/password/email') }}">
                    {!! csrf_field() !!}


                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{trans('auth.email')}}">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>


                    <div class="row">
                        <div class="col-xs-4">
                            <a href="{{url('/')}}" class="text-center"><i class="glyphicon glyphicon-arrow-left"></i> {{trans('auth.back')}}</a>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-8">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">
                                <i class="fa fa-btn fa-envelope"></i> Send Password Reset Link
                            </button>
                        </div>
                        <!-- /.col -->
                    </div>

                </form>

            </div>
        </div>
    </div>
@endsection
