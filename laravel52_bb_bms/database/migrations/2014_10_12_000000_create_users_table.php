<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email',150)->unique();
            $table->string('password',250);
            $table->string('remember_token',255)->nullable()->index();

            $table->boolean('is_active')->default(0);
            $table->integer('employee_id')->unsigned()->index();
            $table->integer('current_role_id')->default(1);
            $table->integer('current_language_id')->default(1);
            $table->timestamp('last_activity');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
