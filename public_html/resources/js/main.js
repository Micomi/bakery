var main = {

    ready: function() {

        main.apply.ready();
    },

    resize: function() {
        //console.log('resize');
    },

    apply: {

        ready: function() {
            main.apply.checkboxSquare();
        },

        checkboxSquare: function() {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }

    }

};

$(main.ready);
//$(window).on('resize', main.resize);